﻿using Owin;

namespace WebApiBoilerplate.IISHost
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Load stuff from web.config here

            // Initialize and bind the api
            var webApiBoilerplate = new WebApiBoilerplate();
            app.UseWebApi(webApiBoilerplate, "/");
        }
    }
}