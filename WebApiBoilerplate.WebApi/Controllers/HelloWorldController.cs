﻿using System.Web.Http;

namespace WebApiBoilerplate.Controllers
{
    public class HelloWorldController : ApiController
    {
        [HttpGet]
        public string SayHello()
        {
            var api = Request.GetWebApiContext();
            return api.SayHello();
        }
    }
}
