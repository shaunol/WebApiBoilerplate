﻿using System;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Cors;
using Owin;

namespace WebApiBoilerplate
{
    public static class OwinExtensions
    {
        /// <summary>
        /// Used for getting and setting the OWIN environment variable which contains a WebApi instance.
        /// </summary>
        internal const string WebApiEnvKey = "webapi.WebApiBoilerplate";

        public static IAppBuilder UseWebApi(this IAppBuilder appBuilder, IWebApiBoilerplate webApi, string virtualPath)
        {
            if (appBuilder == null)
            {
                throw new ArgumentNullException(nameof(appBuilder));
            }

            if (virtualPath == null)
            {
                throw new ArgumentNullException(nameof(virtualPath));
            }

            if (webApi == null)
            {
                throw new ArgumentNullException(nameof(webApi));
            }

            // OWIN related, research exact implications
            appBuilder.Properties["host.AppMode"] = "development";

            // Enable cross origin requests to any requester
            // This should be more restrictive if possible
            appBuilder.UseCors(CorsOptions.AllowAll);

            // Rather than using a DI container, we'll
            // put our application API in the OWIN request
            // context. We provide an extension method for the
            // HttpRequestMessage object to easily access this
            // in a typed fashion without knowing the env key.
            appBuilder.Use(async (context, next) =>
                {
                    context.Set(WebApiEnvKey, webApi);
                    await next();
                }
            );

            // ASP.NET WebApi Route Configuration
            var config = new HttpConfiguration();

            // I personally prefer controllers with actions
            // You can also use REST style controllers
            // See more on WebApi routing:
            // http://www.asp.net/web-api/overview/web-api-routing-and-actions/routing-in-aspnet-web-api
            config.Routes.MapHttpRoute(
                name: "WebApiBoilerplate",
                routeTemplate: String.Concat(
                    virtualPath.TrimStart('/'), 
                    !virtualPath.EndsWith("/") ? "/" : "", 
                    "{controller}/{action}/{id}"),
                defaults: new { id = RouteParameter.Optional }
            );

            // Disable Xml at the moment, we don't actually want to do this
            // Will be replaced ASAP, I just want JSON only right now
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Local error output
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;

            // ASP.NET WebApi also has OWIN extensions
            appBuilder.UseWebApi(config);

            return appBuilder;
        }

        /// <summary>
        /// Get the current instance of the WebApi context.
        /// </summary>
        /// <param name="httpRequest">The HttpRequestMessage instance which contains the desired WebApi context.</param>
        /// <returns>The WebApi instance contained within the provided HttpRequestMessage.</returns>
        public static IWebApiBoilerplate GetWebApiContext(this HttpRequestMessage httpRequest)
        {
            if (httpRequest == null)
            {
                throw new ArgumentNullException(nameof(httpRequest));
            }

            var context = httpRequest.GetOwinContext();
            var repository = context.Get<IWebApiBoilerplate>(WebApiEnvKey);
            return repository;
        }
    }
}
